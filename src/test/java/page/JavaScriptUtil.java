package page;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class JavaScriptUtil {
    public static void drawBorder(WebElement element, JavascriptExecutor js) {
        js.executeScript("arguments[0].style.border='3px solid red'", element);
    }

    public static void clickElementByJS(WebElement element, JavascriptExecutor js) {
        js.executeScript("arguments[0].click()", element);
    }

    public static void generateAlert(JavascriptExecutor js, String message) {
        js.executeScript("alert('" + message + "')");
    }

    public static void refreshBrowser(JavascriptExecutor js) {
        js.executeScript("history.go(0)");
    }

    public static void zoomPageByJS(JavascriptExecutor js) {
        js.executeScript("document.body.style.zoom='50%'");
    }
}
