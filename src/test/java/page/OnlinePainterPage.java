package page;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;

public class OnlinePainterPage {
    private final WebDriver driver;
    private static final String PAGE_LINK = "https://jspaint.app/";
    private final Actions actions;
    private final JavascriptExecutor jsExecutorDriver;

    @FindBy(className = "main-canvas")
    private WebElement canvas;

    @FindBy(css = "div[title='Fill With Color']")
    private WebElement fillWithColorBtn;

    public OnlinePainterPage(WebDriver driver) {
        this.driver = driver;
        actions = new Actions(driver);
        PageFactory.initElements(driver, this);
        jsExecutorDriver = (JavascriptExecutor) driver;
    }

    public OnlinePainterPage openPage() {
        driver.get(PAGE_LINK);
        return this;
    }

    public OnlinePainterPage drawSquare() {

        actions.moveToElement(canvas)
                .clickAndHold()
                .moveByOffset(100, 0)
                .moveByOffset(0, 100)
                .moveByOffset(-100, 0)
                .moveByOffset(0, -100)
                .release()
                .perform();
        return this;
    }

    public OnlinePainterPage clickOnFillInColorByJSExecutor() {
        JavascriptExecutor driver1 = (JavascriptExecutor) driver;
        JavaScriptUtil.clickElementByJS(fillWithColorBtn, driver1);
        return this;
    }

    public OnlinePainterPage fillInColorForPaintedFigure() {
        actions.moveToElement(canvas)
                .moveByOffset(50, 50)
                .click()
                .release()
                .perform();
        return this;
    }

    public OnlinePainterPage drawBorderOfFillInBtn() {
        JavaScriptUtil.drawBorder(fillWithColorBtn, jsExecutorDriver);
        return this;

    }

    public OnlinePainterPage takeScreenshot() throws IOException {
        TakesScreenshot screenshot = (TakesScreenshot) driver;
        File source = screenshot.getScreenshotAs(OutputType.FILE);
        File target = new File(".\\screenshots\\logo.png");
        FileUtils.copyFile(source, target);
        return this;
    }

    public OnlinePainterPage showAnAlertByJSExecutor(String alertMessage) {
        JavaScriptUtil.generateAlert(((JavascriptExecutor) driver), alertMessage);
        return this;
    }

    public OnlinePainterPage refreshPageByJSExecutor() {
        JavaScriptUtil.refreshBrowser(jsExecutorDriver);
        return this;
    }

    public OnlinePainterPage zoomPageByJSExecutor() {
        JavaScriptUtil.zoomPageByJS(jsExecutorDriver);
        return this;
    }
}
