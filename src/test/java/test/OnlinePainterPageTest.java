package test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import page.OnlinePainterPage;

import java.io.IOException;

@DisplayName("Test of online painter")
public class OnlinePainterPageTest {
    private static WebDriver driver;
    private static OnlinePainterPage onlinePainterPage;
    private static final String ALERT_MESSAGE = "The page was updated and zoomed - this is my Selenium Black Square!";

    @BeforeAll
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        onlinePainterPage = new OnlinePainterPage(driver);
    }

    @Test
    @DisplayName("Test of drawing a black square")
    void testOnlinePainter() throws IOException {
        onlinePainterPage.openPage()
                            .drawSquare()
                            .clickOnFillInColorByJSExecutor()
                            .fillInColorForPaintedFigure()
                            .drawBorderOfFillInBtn()
                            .takeScreenshot()
                            .refreshPageByJSExecutor()
                            .zoomPageByJSExecutor()
                            .showAnAlertByJSExecutor(ALERT_MESSAGE);
    }

    @AfterAll
    public static void finishTest() {
        driver.quit();
    }
}
